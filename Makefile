BOLD := \033[1m
RESET := \033[0m

update:
	curl --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" "https://gitlab.com/api/v4/projects/17984557/repository/files/Makefile/raw?ref=master" > Makefile

update_precommit:
	curl --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" "https://gitlab.com/api/v4/projects/17984557/repository/files/.pre-commit-config.yaml/raw?ref=master" > .pre-commit-config.yaml

.PHONY: lint
lint:  ## Run all linters
lint: flake8

.PHONY: flake8
flake8:  ## Run the flake8 tool
	@echo "$(BOLD)Running flake8$(RESET)"
	@poetry run flake8 `find . -name node_modules -prune -o -name .venv -prune -o -name '*.py' -print`

.PHONY: pretty
pretty:  ## Run all code beautifiers (isort, black)
pretty: isort black djhtml

.PHONY: isort
isort:  ## Run the isort tool and update files that need to
	@echo "$(BOLD)Running isort$(RESET)"
	@poetry run isort `find . -name node_modules -prune -o -name .venv -prune -o -name '*.py' -print`

.PHONY: black
black:  ## Run the black tool and update files that need to
	@echo "$(BOLD)Running black$(RESET)"
	@poetry run black .

.PHONY: djhtml
djhtml:  ## Run the djhtml tool
	@echo "$(BOLD)Running djhtml$(RESET)"
	@poetry run djhtml --in-place --tabwidth=2 `find . -name node_modules -prune -o -name .venv -prune -o -name '*.html' -print`

