Changelog
=========


v6.4.1 (2025-01-20)
-------------------

Bug fixes
~~~~~~~~~
- Display error in plugin if no quiz is selected instead of throwing an
  exception [Corentin Bettiol]

Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v6.4.0 (2025-01-15)
-------------------

Features
~~~~~~~~
- Improve quiz admin [Corentin Bettiol]

  - add question number on quiz admin
  - remove quiz name in question __str__ (since questions are only accessed from quiz update form as inlines


Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v6.3.0 (2024-06-26)
-------------------

Features
~~~~~~~~
- Add setting to define logo name in pdf [Corentin Bettiol]

Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v6.2.0 (2024-06-24)
-------------------

Features
~~~~~~~~
- Add "random answers" option in admin [Corentin Bettiol]

  - previous option was to shuffle answers every time
  - you now have to check the "random answers" option in your qui


Documentation
~~~~~~~~~~~~~
- Update README [Corentin Bettiol]
- Update changelog [Corentin Bettiol]


v6.1.1 (2024-06-12)
-------------------

Bug fixes
~~~~~~~~~
- Show show_score_summary option in admin [Corentin Bettiol]

Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v6.1.0 (2024-06-12)
-------------------

Features
~~~~~~~~
- Add "show_score_summary" in weighted answer quiz config [Corentin
  Bettiol]

  - default is True: will display the score in the summary
  - add quizzes descriptions in base template


Bug fixes
~~~~~~~~~
- Typo [Corentin Bettiol]

Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v6.0.2 (2024-06-07)
-------------------

Bug fixes
~~~~~~~~~
- Remove useless constraint in migration [Corentin Bettiol]

Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v6.0.1 (2024-06-04)
-------------------

Bug fixes
~~~~~~~~~
- Fix more info quis on interpretation quiz [Corentin Bettiol]
- Fix interpretation results page [Corentin Bettiol]

  - add pdf link
  - keep max symbol (if save quiz only) after submitting a "more info quiz


Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v6.0.0 (2024-06-04)
-------------------

Features
~~~~~~~~
- Add DJANGO_EASY_QUIZ_PDF_FILE_NAME setting. [Corentin Bettiol]

  - update fr translations
  - do not allow object creation for SavedQuiz (server error


Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]

Maintenance
~~~~~~~~~~~
- Update application [Corentin Bettiol]

  - remove stdv mentions
  - add cms-related content in try/except
  - add 'get_absolute_url' function in models (go to instance from admin



v5.1.3 (2024-01-17)
-------------------

Bug fixes
~~~~~~~~~
- Templates [Corentin Bettiol]

Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v5.1.2 (2024-01-17)
-------------------

Bug fixes
~~~~~~~~~
- Template [Corentin Bettiol]

Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v5.1.1 (2024-01-17)
-------------------

Bug fixes
~~~~~~~~~
- Fix error [Corentin Bettiol]

  missing load i18n in a base template fil


Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v5.1.0 (2023-10-18)
-------------------

Features
~~~~~~~~
- New option to relaunch quiz! [Corentin Bettiol]

  - add new DJANGO_EASY_QUIZ_RELAUNCH_BUTTON option that displays a link
  letting users restart the quiz.
  - update translations
  - FIX: error when no conclusion for nb of points
  - FIX: fix empty conclusion problem
  - FIX: fix .isdigit bug
  - FIX: fix autocomplete form bu


Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v5.0.0 (2023-10-10)
-------------------

Features
~~~~~~~~
- New settings! new requirements! [Corentin Bettiol]

  - rename DJANGO_EASY_QUIZ_GATHER_STATISTICS_END ->
  DJANGO_EASY_QUIZ_GATHER_STATISTICS
  - add setting GATHER_STATISTICS_DURING_QUIZ
  - update README.md
  - move settings in new file settings.py
  - allow export of saved quizzes using django-import-expor


Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v4.2.2 (2023-10-09)
-------------------

Bug fixes
~~~~~~~~~
- Fix django warning [Corentin Bettiol]

  '(fields.E010) JSONField default should be a callable instead of an instance


Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v4.2.1 (2023-10-03)
-------------------

Bug fixes
~~~~~~~~~
- Fix compatibility with pango 1.0.0 [Corentin Bettiol]

  - pango 1.44 is not available on debian 10 that is still widely used on servers. So stick to weasyprint v52.


Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v4.1.0 (2023-10-03)
-------------------

Features
~~~~~~~~
- Add 'is_list' in more info quiz options [Corentin Bettiol]

  - if checked, this option will display options using a <select> instead
  or radio buttons.
  - update translation


Documentation
~~~~~~~~~~~~~
- Update package description in pyproject.toml [Corentin Bettiol]
- Fix typo [Corentin Bettiol]
- Update changelog [Corentin Bettiol]


v4.0.2 (2023-10-03)
-------------------

Bug fixes
~~~~~~~~~
- Fix logo repository url in pyproject.toml [Corentin Bettiol]

Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v4.0.1 (2023-10-03)
-------------------

Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]

Maintenance
~~~~~~~~~~~
- Update dependency to djangodjango-ckeditor-filebrowser-filer [Corentin
  Bettiol]


v4.0.0 (2023-10-03)
-------------------

Features
~~~~~~~~
- Improve interpretationquizzes [Corentin Bettiol]

  - move more_info_quiz to its own template
  - add support for saved_quizzes to interpretation_quiz objects
  - add support for more_infos_quizzes to interpretation_quiz objects
  - add support for pdf files to interpretation_quiz object

- Lot of things! [Corentin Bettiol]

  - add SAVE_QUIZZES_RESULTS settings (the app will save results of
  quizzes)
  - add GATHER_STATISTICS_END settings (you can add new 'small quizzes'
  that will be displayed once you have finished the main quiz, in
  order to ask for more info
  - add SAVE_PDF settings (only if SAVE_QUIZZES_RESULTS is set to True)
  - update translation

- Add dl pdf view ! [Corentin Bettiol]

  I will need to clean this and put the relevant logic in the project
  that will use this feature. But in the meantime, you can use this
  branch to do what you want


Bug fixes
~~~~~~~~~
- Fix translations 'result' [Corentin Bettiol]

Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]

Maintenance
~~~~~~~~~~~
- Update class on template [Corentin Bettiol]

  - add help text on models
  - update translations
  - remove unused template fil



v3.0.1 (2023-07-13)
-------------------

Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]

Maintenance
~~~~~~~~~~~
- Forgot to compile translations [Corentin Bettiol]


v3.0.0 (2023-07-13)
-------------------

Bug fixes
~~~~~~~~~
- Allow selecting zero answer [Corentin Bettiol]

  ... on weighted answers quiz.

  Update translations


Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]

Maintenance
~~~~~~~~~~~
- Fix models, reinit migrations [Corentin Bettiol]


v2.2.0 (2023-07-12)
-------------------

Features
~~~~~~~~
- Add quiz summary in conclusion [Corentin Bettiol]

  ... only for weighted answers quiz.

  Add multiple answers mention if answer accept... multile answers.
  Update translations


Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v2.1.1 (2023-07-12)
-------------------

Bug fixes
~~~~~~~~~
- If checkbox then not required [Corentin Bettiol]

  All checkboxes had the required tag, but since they are checkboxes you
  need to be able to no check any answer


Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v2.1.0 (2023-07-12)
-------------------

Features
~~~~~~~~
- Multiple answers per question (facultative) [Corentin Bettiol]

  - update models (add multiple_answers boolean field)
  - add migration
  - update views to handle multiple questions
  - update default template to display checkboxes instead of labe


Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v2.0.2 (2023-07-12)
-------------------

Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]

Maintenance
~~~~~~~~~~~
- Add 2 questions in weighted_answers quizzes [Corentin Bettiol]


v2.0.1 (2023-07-12)
-------------------

Bug fixes
~~~~~~~~~
- Fix poetry-core dependency version [Corentin Bettiol]

Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v2.0.0 (2023-07-12)
-------------------

Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]

Maintenance
~~~~~~~~~~~
- Rewrite models & admin, update templates [Corentin Bettiol]

  We decided to rewrite the app's models & admin in order to increase
  ergonomy.

  You'll need to run migrate zero, and then re-migrate the app after
  updating. But by doing so you will lose any already created quiz,
  so be careful



v1.0.0 (2023-07-06)
-------------------

Documentation
~~~~~~~~~~~~~
- Update README [Corentin Bettiol]
- Update README [Corentin Bettiol]

Maintenance
~~~~~~~~~~~
- Add required to radio buttons [Corentin Bettiol]
- Add migrations, fix templates [Corentin Bettiol]
- Rename repo + other updates [Corentin Bettiol]

  - move content in plugins model



0.0.1 (2023-06-27)
------------------

Features
~~~~~~~~
- First commit! [Corentin Bettiol]




.. Generated by gitchangelog
